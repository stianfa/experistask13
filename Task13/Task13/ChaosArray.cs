﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13
{
    class ChaosArray<T>
    {
       
        private T[] arr;                    // Main array to keep the objects
        private T def = default(T);         // Default type to check for empty spots
        Random rand = new Random();         // Random object to produce random numbers

        public ChaosArray(int capacity)   
        {
            arr = new T[capacity];

            for (int i = 0; i < capacity; i++)
            {
                arr[i] = def;
            }
        
        }

        // Insert item into a random spot in the array
        public void InsertChaos(T item)
        {
            int r = rand.Next(arr.Length-1);

            if(arr[r] == null || arr[r].Equals(def))
            {
                arr[r] = item;
            }
            else
            {
                throw new UnavailableSpaceException("No available space");
            }
        }

        // Get an item from a random spot in the array
        public T GetChaos()
        {
            int r = rand.Next(arr.Length - 1);

            T item = arr[r];

            if(!item.Equals(def))
            {
                return item;
            }
            else
            {
                throw new ItemNotThereException("No item there");
            }
        }

        // Print current positions and values in the array
        public void printChaos()
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine($"value at index {i}: {arr[i]}");
            }
        }

    }
}
