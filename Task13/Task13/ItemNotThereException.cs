﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13
{
    class ItemNotThereException : Exception
    {
        public ItemNotThereException() { }

        public ItemNotThereException(string message) : base(message)
        {

        }
    }
}
