﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13
{
    class UnavailableSpaceException : Exception
    {
        public UnavailableSpaceException() { }

        public UnavailableSpaceException(string message) : base(message)
        {

        }
    }
}
