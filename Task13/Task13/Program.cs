﻿using System;

namespace Task13
{
    class Program
    {
        static void Main(string[] args)
        {
            ChaosArray<int> chaos = new ChaosArray<int>(15);            

            try
            {

                // Insert values into the chaosarray
                for (int i = 0; i < 10; i++)
                {
                    try
                    {
                        chaos.InsertChaos(i);
                    }
                    catch (UnavailableSpaceException ex)
                    {
                        Console.WriteLine("UnavailableSpaceException caught: " + ex);
                    }
                }

                // Print the array afer values have been inserted
                chaos.printChaos();

                // Try to retrieve values from the array
                for (int i = 0; i < 10; i++)
                {
                    try
                    {
                        chaos.GetChaos();
                        Console.WriteLine("Succesfully retrieved item");
                    }
                    catch (ItemNotThereException ex)
                    {
                        Console.WriteLine("ItemNotThereException caught " + ex);
                    }
                }
            }         
            catch (Exception ex)
            {
                Console.WriteLine("General Exception caught: " + ex);
            }
            finally
            {
                Console.WriteLine("That's all folks");
            }

        }
    }
}
